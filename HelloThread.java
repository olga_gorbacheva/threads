package ruu.god.hellothreads;

/**
 * Класс, демонстрирующий создание 10-ти потоков
 *
 * @author Горбаечва, 16ИТ18к
 */
public class HelloThread {
    public static void main(String[] args) {
        for (int i = 10; i > 0; i--) {
            System.out.println(i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Поток завершен");
            }
        }
    }
}
